package anangkur.com.testmodulkotlinanko

import anangkur.com.testmodulkotlinanko.R.color.colorAccent
import anangkur.com.testmodulkotlinanko.R.color.design_snackbar_background_color
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.View
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUI().setContentView(this)
        /*setContentView(R.layout.activity_main)*/
    }

    class MainActivityUI : AnkoComponent<MainActivity>{
        override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
            verticalLayout {
                padding = dip(16)

                val name = editText{
                    hint = "who is your name?"
                }

                button("say hello"){
                    backgroundColor = ContextCompat.getColor(context, colorAccent)
                    textColor = Color.WHITE
                    onClick { toast("hello, ${name.text}!") }
                }.lparams(width = matchParent){
                    topMargin = dip(5)
                }

                button("show allert"){
                    backgroundColor = ContextCompat.getColor(context, colorAccent)
                    textColor = Color.WHITE
                    onClick { alert ("happy coding ${name.text}!"){
                        yesButton { toast("yes..") }
                        noButton { toast("no..") }
                    }.show() }
                }.lparams(width = matchParent){
                    topMargin = dip(5)
                }

                button("show selector"){
                    backgroundColor = ContextCompat.getColor(context, colorAccent)
                    textColor = Color.WHITE
                    onClick {
                        val clubs = listOf("barcelona", "ac milan", "real madrid", "liverpool", "chelsea", "arsenal", "manchester united")
                        selector("hallo ${name.text} what football club do you like?", clubs){
                            dialogInterface, i -> toast("so you love ${clubs[i]}, right?")
                        }
                    }
                }.lparams(width = matchParent){
                    topMargin = dip(5)
                }

                button("show snackbar"){
                    backgroundColor = ContextCompat.getColor(context, colorAccent)
                    textColor = Color.WHITE
                    onClick {
                        snackbar(name, "hello ${name.text}!")
                    }
                }.lparams(width = matchParent){
                    topMargin = dip(5)
                }

                button("show progress bar"){
                    backgroundColor = ContextCompat.getColor(context, colorAccent)
                    textColor = Color.WHITE
                    onClick {
                        indeterminateProgressDialog("hello ${name.text} please wait..").show()
                    }
                }.lparams(width = matchParent){
                    topMargin = dip(5)
                }

                button("go to second activity"){
                    backgroundColor = ContextCompat.getColor(context, colorAccent)
                    textColor = Color.WHITE
                    onClick {
                        startActivity<SecondActivity>("name" to "${name.text}")
                    }
                }.lparams(width = matchParent){
                    topMargin = dip(5)
                }
            }
        }

    }
}
